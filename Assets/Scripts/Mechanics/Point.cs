﻿using System;
using UnityEngine;

namespace Mechanics
{
    [Serializable]
    public class Point
    {
        public int x;
        public int y;

        public Point(int nx, int ny)
        {
            x = nx;
            y = ny;
        }

        public void Mult(int n)
        {
            x *= n;
            y *= n;
        }
        
        public void Add(Point p)
        {
            x += p.x;
            y += p.y;
        }

        public Vector2 ToVector() => new Vector2(x, y);

        public bool Equals(Point p) => x == p.x && y == p.y;

        public static Point FromVector(Vector2 v) => new Point((int) v.x, (int) v.y);

        public static Point FromVector(Vector3 v) => new Point((int) v.x, (int) v.y);

        public static Point Mult(Point p, int n) => new Point(p.x * n, p.y * n);
        
        public static Point Add(Point p, Point p2) => new Point(p.x + p2.x, p.y + p2.y);

        public static Point Clone(Point point) => new Point(point.x, point.y);

        public static Point Zero => new Point(0, 0);
        public static Point One => new Point(1, 1);
        public static Point Up => new Point(0, 1);
        public static Point Down => new Point(0, -1);
        public static Point Left => new Point(-1, 0);
        public static Point Right => new Point(1, 0);
        
    }
}