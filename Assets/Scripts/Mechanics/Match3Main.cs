using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Mechanics
{
    public class Match3Main : MonoBehaviour
    {
        private const int Width = 9;
        private const int Height = 14;
        public const int PieceSize = 64;

        [Header("Ui Elements")] public RectTransform gameBoard;
        public Sprite[] pieces;
        public BoardHoles boardHoles;

        [Header("Prefabs")] public GameObject nodePiece;
        private int[] fills;
        private Node[,] _board;
        private Random _rand;
        private List<NodePiece> _updatePieces;
        private List<FlippedPieces> _flippedPieces;
        private List<NodePiece> _deadPieces;


        private void Start() =>
            StartGame();

        private void Update()
        {
            List<NodePiece> finishedUpdating = new List<NodePiece>();
            for (int i = 0; i < _updatePieces.Count; i++)
            {
                NodePiece piece = _updatePieces[i];
                if (!piece.UpdatePiece()) finishedUpdating.Add(piece);
            }

            for (int i = 0; i < finishedUpdating.Count; i++)
            {
                NodePiece piece = finishedUpdating[i];
                FlippedPieces flip = GetFlippedPiece(piece);
                NodePiece flippedPiece = null;

                int x = piece.index.x;
                fills[x] = Mathf.Clamp(fills[x] - 1, 0, Width);

                List<Point> connected = IsConnected(piece.index, true);
                Debug.Log(connected.Count);
                bool wasFlipped = flip != null;

                if (wasFlipped) //If we flipped to make this update
                {
                    flippedPiece = flip.GetOtherPiece(piece);
                    AddPoints(ref connected, IsConnected(flippedPiece.index, true));
                }

                if (connected.Count == 0) //If we didn't make a match
                {
                    if (wasFlipped) //If we flipped
                        FlipPieces(piece.index, flippedPiece.index, false); //Flip back
                }
                else //If we made a match
                {
                    foreach (Point pnt in connected) //Remove the node pieces connected
                    {
                        Node node = GetNodeAtPoint(pnt);
                        NodePiece nodePiece = node.GetPiece();
                        if (nodePiece != null)
                        {
                            nodePiece.gameObject.SetActive(false);
                            _deadPieces.Add(nodePiece);
                        }

                        node.SetPiece(null);
                    }

                    ApplyBoardGravity();
                }

                _flippedPieces.Remove(flip); //Remove the flip after update
                _updatePieces.Remove(piece);
            }
        }

        private FlippedPieces GetFlippedPiece(NodePiece p)
        {
            FlippedPieces flip = null;
            for (int i = 0; i < _flippedPieces.Count; i++)
            {
                if (_flippedPieces[i].GetOtherPiece(p) != null)
                {
                    flip = _flippedPieces[i];
                    break;
                }
            }

            return flip;
        }

        public Vector2 GetPositionFromPoint(Point p)
        {
            return new Vector2(PieceSize / 2 + PieceSize * p.x, -PieceSize / 2 - PieceSize * p.y);
        }

        public void ResetPiece(NodePiece piece)
        {
            piece.ResetPosition();
            _updatePieces.Add(piece);
        }

        public void FlipPieces(Point ind1, Point ind2, bool main)
        {
            if (GetValueAtPoint(ind1) < 0) return;

            Node node1 = GetNodeAtPoint(ind1);
            NodePiece piece1 = node1.GetPiece();
            if (GetValueAtPoint(ind2) > 0)
            {
                Node node2 = GetNodeAtPoint(ind2);
                NodePiece piece2 = node2.GetPiece();
                node1.SetPiece(piece2);
                node2.SetPiece(piece1);

                if (main)
                    _flippedPieces.Add(new FlippedPieces(piece1, piece2));

                _updatePieces.Add(piece1);
                _updatePieces.Add(piece2);
            }
            else
                ResetPiece(piece1);
        }

        private void StartGame()
        {
            string seed = GetRandomSeed();
            _rand = new Random(seed.GetHashCode());
            _updatePieces = new List<NodePiece>();
            _flippedPieces = new List<FlippedPieces>();
            _deadPieces = new List<NodePiece>();
            fills = new int[Width];

            InitializeBoard();
            VerifyBoard();
            InstantiateBoard();
        }

        private void InitializeBoard()
        {
            _board = new Node[Width, Height];
            for (var y = 0; y < Height; y++)
            for (var x = 0; x < Width; x++)
                _board[x, y] = new Node(boardHoles.boardLayout.rows[y].row[x] ? -1 : FillPiece(), new Point(x, y));
        }

        private void VerifyBoard()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Point p = new Point(x, y);
                    int val = GetValueAtPoint(p);
                    if (val <= 0) continue;

                    var remove = new List<int>();
                    while (IsConnected(p, true).Count > 0)
                    {
                        val = GetValueAtPoint(p);
                        if (!remove.Contains(val))
                            remove.Add(val);
                        SetValueAtPoint(p, NewValue(ref remove));
                    }
                }
            }
        }

        private void InstantiateBoard()
        {
            for (var x = 0; x < Width; x++)
            for (var y = 0; y < Height; y++)
            {
                Node node = GetNodeAtPoint(new Point(x, y));

                int val = node.value;
                if (val <= 0) // it is hole 
                    continue;
                var p = Instantiate(nodePiece, gameBoard);
                NodePiece piece = p.GetComponent<NodePiece>();
                var rect = p.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(PieceSize / 2 + PieceSize * x, -PieceSize / 2 - PieceSize * y);
                piece.Initialize(val, new Point(x, y), pieces[val - 1]);
                node.SetPiece(piece);
            }
        }

        private void ApplyBoardGravity()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = Height - 1; y >= 0; y--)
                {
                    Point p = new Point(x, y);
                    Node node = GetNodeAtPoint(p);
                    int val = GetValueAtPoint(p);
                    if (val != 0) // it is not a hole
                        continue;
                    for (int ny = y - 1; ny >= -1; ny--)
                    {
                        Point next = new Point(x, ny);
                        int nextV = GetValueAtPoint(next);
                        if (nextV == 0) // it's a hole
                            continue;
                        if (
                            nextV !=
                            -1) // if we did not hit an end, but its not a 0 then use this to fill the current hole
                        {
                            Node got = GetNodeAtPoint(next);
                            NodePiece piece = got.GetPiece();

                            //Set the hole
                            node.SetPiece(piece);
                            _updatePieces.Add(piece);

                            //Replace the hole
                            got.SetPiece(null);
                        }
                        else //Use dead ones or create new piece to fill holes (hit a -1) only if we choose to 
                        {
                            //Fill in the hole
                            int newVal = FillPiece();
                            NodePiece piece;
                            Point fallP = new Point(x, -1 - fills[x]);

                            if (_deadPieces.Count > 0)
                            {
                                NodePiece revived = _deadPieces[0];
                                revived.gameObject.SetActive(true);
                                revived.rect.anchoredPosition = GetPositionFromPoint(fallP);
                                piece = revived;

                                _deadPieces.RemoveAt(0);
                            }
                            else
                            {
                                GameObject obj = Instantiate(nodePiece, gameBoard);
                                NodePiece n = obj.GetComponent<NodePiece>();
                                RectTransform rect = obj.GetComponent<RectTransform>();
                                rect.anchoredPosition = GetPositionFromPoint(fallP);
                                n.Initialize(newVal, p, pieces[newVal - 1]);
                                piece = n;
                            }

                            piece.Initialize(newVal, p, pieces[newVal - 1]);
                            Node hole = GetNodeAtPoint(p);
                            hole.SetPiece(piece);
                            ResetPiece(piece);
                            fills[x]++;
                        }

                        break;
                    }
                }
            }
        }

        private int NewValue(ref List<int> remove)
        {
            var available = new List<int>();
            for (var i = 0; i < pieces.Length; i++)
                available.Add(i + 1);
            foreach (int i in remove)
                available.Remove(i);

            if (available.Count <= 0)
                return 0;
            return available[_rand.Next(0, available.Count)];
        }

        /// <summary>
        ///     Get List of all connections
        /// </summary>
        /// <param name="p">Current point</param>
        /// <param name="isMain">if it's main - use recursive</param>
        /// <returns></returns>
        private List<Point> IsConnected(Point p, bool isMain)
        {
            List<Point> connected = new List<Point>();
            int val = GetValueAtPoint(p);
            Point[] directions =
            {
                Point.Up,
                Point.Right,
                Point.Down,
                Point.Left
            };

            foreach (Point dir in directions) //Checking if there is 2 or more same shapes in the directions
            {
                List<Point> line = new List<Point>();

                int same = 0;
                for (int i = 1; i < 3; i++)
                {
                    Point check = Point.Add(p, Point.Mult(dir, i));
                    if (GetValueAtPoint(check) == val)
                    {
                        line.Add(check);
                        same++;
                    }
                }

                if (same > 1) //If there are more than 1 of the same shape in the direction then we know it is a match
                    AddPoints(ref connected, line); //Add these points to the overarching connected list
            }

            for (int i = 0; i < 2; i++) //Checking if we are in the middle of two of the same shapes
            {
                List<Point> line = new List<Point>();

                int same = 0;
                Point[] check = {Point.Add(p, directions[i]), Point.Add(p, directions[i + 2])};
                foreach (Point next in
                    check) //Check both sides of the piece, if they are the same value, add them to the list
                {
                    if (GetValueAtPoint(next) == val)
                    {
                        line.Add(next);
                        same++;
                    }
                }

                if (same > 1)
                    AddPoints(ref connected, line);
            }

            for (int i = 0; i < 4; i++) //Check for a 2x2
            {
                List<Point> square = new List<Point>();

                int same = 0;
                int next = i + 1;
                if (next >= 4)
                    next -= 4;

                Point[] check =
                {
                    Point.Add(p, directions[i]), Point.Add(p, directions[next]),
                    Point.Add(p, Point.Add(directions[i], directions[next]))
                };
                foreach (Point pnt in
                    check) //Check all sides of the piece, if they are the same value, add them to the list
                {
                    if (GetValueAtPoint(pnt) == val)
                    {
                        square.Add(pnt);
                        same++;
                    }
                }

                if (same > 2)
                    AddPoints(ref connected, square);
            }

            if (isMain) //Checks for other matches along the current match
            {
                for (int i = 0; i < connected.Count; i++)
                    AddPoints(ref connected, IsConnected(connected[i], false));
            }

            /* UNNESSASARY | REMOVE THIS!
        if (connected.Count > 0)
            connected.Add(p);
        */

            return connected;
        }

        private static void AddPoints(ref List<Point> points, List<Point> add)
        {
            foreach (Point p in add)
            {
                bool doAdd = true;

                foreach (var point in points)
                {
                    if (point.Equals(p))
                    {
                        doAdd = false;
                        break;
                    }
                }

                if (doAdd)
                    points.Add(p);
            }
        }

        /// <summary>
        ///     Get shape type inside point
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private int GetValueAtPoint(Point p)
        {
            if (p.x < 0 || p.x >= Width || p.y < 0 || p.y >= Height)
                return -1;
            return _board[p.x, p.y].value;
        }

        private Node GetNodeAtPoint(Point point) =>
            _board[point.x, point.y];

        private void SetValueAtPoint(Point p, int v) =>
            _board[p.x, p.y].value = v;

        private int FillPiece()
        {
            int val = _rand.Next(0, 100) / (100 / pieces.Length) + 1;
            return val;
        }

        private static string GetRandomSeed()
        {
            string seed = "";
            string acceptableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdeghijklmnopqrstuvwxyz1234567890!@#$%^&*()";
            for (int i = 0; i < 20; i++)
                seed += acceptableChars[UnityEngine.Random.Range(0, acceptableChars.Length)];
            return seed;
        }
    }

    [Serializable]
    public class FlippedPieces
    {
        public NodePiece one;
        public NodePiece two;

        public FlippedPieces(NodePiece one, NodePiece two)
        {
            this.one = one;
            this.two = two;
        }

        public NodePiece GetOtherPiece(NodePiece p)
        {
            if (p == one)
                return two;
            else if (p == two)
                return one;
            else
                return null;
        }
    }
}