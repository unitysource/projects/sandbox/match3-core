﻿using UnityEngine;

namespace Mechanics
{
	[System.Serializable]
	public class ArrayLayout
	{

		[System.Serializable]
		public struct RowData
		{
			public bool[] row;
		}

		public Grid grid;
		public RowData[] rows = new RowData[14]; //Grid of 7x7
	}
}
