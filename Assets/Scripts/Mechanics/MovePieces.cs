﻿using UnityEngine;

namespace Mechanics
{
    public class MovePieces : MonoBehaviour
    {
        public static MovePieces Instance;
        [SerializeField] private Match3Main match3Main;

        private NodePiece _moving;
        private Point _newIndex;
        private Vector2 _mouseStart;

        private void Awake() => 
            Instance = this;

        private void Update()
        {
            if (_moving != null) //if we click on any piece
            {
                Vector2 dir = (Vector2) Input.mousePosition - _mouseStart;
                Vector2 nDir = dir.normalized;
                Vector2 aDir = new Vector2(Mathf.Abs(dir.x), Mathf.Abs(dir.y));

                _newIndex = Point.Clone(_moving.index);
                Point add = Point.Zero;
                if (dir.magnitude > 32) //if cursor out from this figure
                {
                    //Make add (0, 1) or (1, 0) or ... depends on the direction of the mouse point 
                    if (aDir.x > aDir.y)
                        add = new Point(nDir.x > 0 ? 1 : -1, 0);
                    else if (aDir.x < aDir.y)
                        add = new Point(0, nDir.y > 0 ? -1 : 1);
                }

                _newIndex.Add(add);

                Vector2 pos = match3Main.GetPositionFromPoint(_moving.index);
                if (!_newIndex.Equals(_moving.index))
                    pos += Point.Mult(new Point(add.x, -add.y), 16).ToVector();
                _moving.MovePositionTo(pos);
            }
        }

        public void MovePiece(NodePiece piece)
        {
            if (_moving != null) return;

            _moving = piece;
            _mouseStart = Input.mousePosition;
        }

        public void DropPiece()
        {
            if (_moving == null) return;
            Debug.Log("Dropped");
            if(!_newIndex.Equals(_moving.index))
                match3Main.FlipPieces(_moving.index, _newIndex, true);
            else 
                match3Main.ResetPiece(_moving);
            _moving = null;
        }
    }
}