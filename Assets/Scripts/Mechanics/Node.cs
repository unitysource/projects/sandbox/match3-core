﻿using System;

namespace Mechanics
{
    [Serializable]
    public class Node 
    {
        public int value; //0 = blank, 1 = cube, 2 = sphere, 3 = cylinder, 4 = pryamid, 5 = diamond, -1 = hole
        public Point index;
        private NodePiece _piece;

        public Node(int v, Point i)
        {
            value = v;
            index = i;
        }

        public void SetPiece(NodePiece p)
        {
            _piece = p;
            value = (_piece == null) ? 0 : _piece.value;
            if (_piece == null) return;
            _piece.SetIndex(index);
        }

        public NodePiece GetPiece()
        {
            return _piece;
        }
    }
}