﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Mechanics
{
    public class NodePiece : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public int value;
        public Point index;

        [HideInInspector] public Vector2 position;
        [HideInInspector] public RectTransform rect;

        private Image _image;
        private bool _updating;

        public void Initialize(int v, Point p, Sprite piece)
        {
            _image = GetComponent<Image>();
            rect = GetComponent<RectTransform>();

            value = v;
            SetIndex(p);
            _image.sprite = piece;
        }

        public void SetIndex(Point p)
        {
            index = p;
            ResetPosition();
            UpdateInspectorName();
        }

        public void ResetPosition()
        {
            const int size = Match3Main.PieceSize;
            position = new Vector2(size / 2 + size * index.x, -size / 2 - size * index.y);
        }

        public void MovePosition(Vector2 move)
        {
            rect.anchoredPosition += move * Time.deltaTime * 16f;
        }
        
        public void MovePositionTo(Vector2 move)
        {
            rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, move, 16f * Time.deltaTime);
        }

        private void UpdateInspectorName() => 
            transform.name = $"Node [{index.x}, {index.y}]";

        public void OnPointerDown(PointerEventData eventData)
        {
            if(_updating) return;
            MovePieces.Instance.MovePiece(this);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            MovePieces.Instance.DropPiece();
        }

        public bool UpdatePiece()
        {
            if (Vector3.Distance(rect.anchoredPosition, position) > 1)
            {
                MovePositionTo(position);
                _updating = true;
                return true;
            }
            
            rect.anchoredPosition = position;
            _updating = false;
            return false;
        }
    }
}